//actions to do if it is an lert
function doWhenAlertDetail() {
	$('#alert-treatment-panel').show();
	$('.answer-table-container .score-score').html('20');
	$('.answer-table-container .score-score').removeClass('score-green');
	$('.answer-table-container .score-score').addClass('score-red');
	$('.client-info-status span').text('Alertant');
	$('.client-info-status img').attr('src','img/red_alarm.png');
};

function doWhenDetail() {
	$('#alert-treatment-panel').hide();
	$('.answer-table-container .score-score').html('100');
	$('.answer-table-container .score-score').removeClass('score-red');
	$('.answer-table-container .score-score').addClass('score-green');
	$('.client-info-status span').text('Ambassadeur');
	$('.client-info-status img').attr('src','img/star.png');
};


$(document).ready(function() {
	$('.surveys-dropdown').show();

	showDetail();

	$('.access-detail').click(function() {
		if ($(this).attr('is-alert') == 'true') {
			$(location).attr('hash', '#detail_alert');
		} else {
			$(location).attr('hash', '#detail');
		}
		
		showDetail();
	});

	$('#closeDetail').click(function() {
		$(location).attr('hash','');
		showDetail();
	});

	$('#contactDate').datepicker();

});


function showDetail() {
	var url = $(location).attr('hash');
	if (url == '#detail' || url == '#detail_alert') {

		// alert or not?
		if (url == '#detail_alert') {
			doWhenAlertDetail();
		} else {
			doWhenDetail();
		}

		$('.show-in-surveys-table, #surveys-table').hide();
		$('#survey-detail').slideDown();
	} else {
		$('#survey-detail').hide();
		$('.show-in-surveys-table, #surveys-table').slideDown();
	}
}