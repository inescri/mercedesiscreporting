function createRepartionBars(container, series) {
		var sumSeries = 0;
		$.each(series, function(i, s) {
				sumSeries = sumSeries + s.data[0];
		})

		$(container).highcharts({
			chart: {
				type: 'column',
				height: 360,
				backgroundColor: 'rgba(0,0,0,0)',
				marginTop: 25
			},
			title: {text: null},
			xAxis: {
				categories: ['R\351ponses']
			},
			yAxis: {
				title: {
						text: ''
				}
			},
			legend: {
				enabled: true,
				symbolRadius: 0,
				itemStyle: {
						fontFamily: 'Arial, sans-serif'
				},
				//layout: 'vertical'
			},
			plotOptions: {
				column: {
						dataLabels: {enabled: true, formatter: function() {
								return Math.round((this.y/sumSeries) * 100) + '%' ;
						}}
				}
			},
			series:series,
			credits: {enabled: false}
		});
}

function createRepartionPie(container, series) {
	$(container).highcharts({
		chart: {
			type: 'pie',
			height: 250,
			backgroundColor: 'rgba(1,1,1,0)'
		},
		title: {
			text: null
		},
		plotOptions: {
			pie: {
				innerSize: '55%',
				showInLegend: true,
				allowPointSelect: true,
				dataLabels: { enabled: true, formatter: function() {
					return this.percent;
				}},
				point: {
					events: {
						click: function() {
							$(container).trigger('pointOnPieCliked', this);
						}
					}
				}
			},
		},
		tooltip: {
			pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.percentage:.0f}% </b> <br> Total <b>{point.total}</b><br/>'
		},
		legend: {
			enabled: true,
			align: 'right',
			layout: 'vertical',
			y: -50,
			labelFormatter: function() {
				return Math.round(this.percentage) + '% : ' + this.name;
								//return this.name;
			},
			symbolRadius: 0,
			itemStyle: {
					fontWeight: 'bold',fontSize: '10pt', fontFamily: 'Arial, sans-serif'
			},
			itemMarginTop: 5
		},
		series: series,
		credits: {enabled: false}
	});
}

function createEvolutionRepartition(container, series) {
	$(container).highcharts({
		chart: {
				type: 'line'
		},
		title: {
				text: null
		},
		series: [{
				name: 'Tout \340 fait satisfait(e)',
				data: [120, 80, 120, 180, 130, 140]
		}],
		credits: {enabled: false}
	});
}

function createComparatifsBar(container, series) {
	$(container).highcharts({
		chart: {
			type: 'bar',
			height: 250,
			backgroundColor: 'rgba(0,0,0,0)',
			marginTop: 25
		},
		title: {
			text: null
		},
		yAxis: {
			title: {
				text: null
			},
		},
		xAxis: {
			categories: ['score']
		},
		series: series,
		credits: {enabled: false}
	});
}

function createComparatifsBar2(container, series, categories) {
	$(container).highcharts({
		chart: {
			type: 'column',
			backgroundColor: 'rgba(0,0,0,0)',
			marginTop: 25,
			height: 340
		},
		title: {
			text: null
		},
		xAxis: {
			categories: categories
		},
		yAxis: {
			title: null
		},
		series: series,
		credits: {enabled: false}
	});
}

function createRepartitionEvo(container, series, clickablePoint, categories) {

	if (typeof categories == 'undefined') {
		categories = ['jan 2', 'jan 3', 'jan 4', 'jan 5', 'jan 6',  'jan 7',  'jan 8',  'jan 9'];
	}

	$(container).highcharts({
		chart: {
			backgroundColor: 'rgba(0,0,0,0)',
			marginTop: 25,
			type: 'area'
		},
		title: {
			text: null
		},
		plotOptions: {
			area: {
				stacking: 'percent',
				point: {
					events: {
						click: function() {
							console.log(this);
							$(container).trigger('pointOnAreaCliked', this);
						}
					}
				}
			 }
		},
		legend: {
			symbolRadius: 0
		},
		tooltip: {
			shared: true,
			pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.percentage:.0f}% </b> ({point.y})<br/>'
		},
		xAxis: {
			categories: categories
		},
		yAxis: {
			title: {
				text: null
			}
		},
		series: series,
		credits: {enabled: false}
	});
}

function createScoresEvo(container, series, categories) {
	if (typeof categories == 'undefined') {
		categories = ['jan 2', 'jan 3', 'jan 4', 'jan 5', 'jan 6',  'jan 7',  'jan 8',  'jan 9'];
	}
	
	$(container).highcharts({
		chart: {
			backgroundColor: 'rgba(0,0,0,0)',
			marginTop: 25,
			type: 'line'
		},
		title: {
			text: null
		},
		plotOptions: {
			area: {
				point: {
					events: {
						click: function() {
							console.log(this);
							$(container).trigger('pointOnAreaCliked', this);
						}
					}
				}
			 }
		},
		legend: {
			symbolRadius: 0
		},
		tooltip: {
			shared: true,
			pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.0f} </b><br/>'
		},
		xAxis: {
			categories: categories
		},
		yAxis: {
			title: {
				text: null
			}
		},
		series: series,
		credits: {enabled: false}
	})
}
