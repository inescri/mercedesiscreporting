$(document).ready(function() {
	$('.score-dropdown').show();
	
	$('body').on("BU_CHANGE", function() {
		
		$('.scores-table').find('th,td').show();
		if (BU_ACTIVE == 'Vente Cars') {
			$('.QA').html('Accueil');
			$('.QB').html('Comp\351tences Vendeur');
			$('.QC').html('D\351roulement de l\'essai');
			$('.QD').html('Explications sur le v\351hicule');
			$('.QE').html('Suivi apres livraison');
			$('.QF').html('Financement et leasing');
		}
		if (BU_ACTIVE == 'Service Cars') {
			$('.QA').html('Prise de Rendez-vous');
			$('.QB').html('Conseiller de clientele SAV');
			$('.QC').html('L\'infrastructure');
			$('.QD').html('R\351cup\351ration du v\351hicule');
			$('.QE').html('Qualit\351 du suivi apres intervention');
			$('.QF').html('');
			$('.RF').html('');
		}
		
		if (BU_ACTIVE == 'Vente Vans') {
			$('.QA').html('Satisfaction globale');
			$('.QB').html('Relationnel vendeur');
			$('.QC').html('Propos d\'essai');
			$('.QD').html('D\351roulement de l\'essai');
			$('.QE').html('Livraison du v\351hicule');
			$('.QF').html('Contact post-livraison');
			$('.QG').html('Suivi apres livraison');
		}
		
		if (BU_ACTIVE == 'Service Trucks' || BU_ACTIVE == 'Vente Trucks' || BU_ACTIVE == 'Service Vans') {
			$('.QA').html('Satisfaction globale');
			$('.QB').html('Recommandation');
			$('.QC').html('Fid\351lit\351');
			$('.QD').html('');
			$('.QE').html('');
			$('.QF').html('');
			$('.RF').html('');
		}
		//getting empty headers
		var hide = [];
		$('.scores-table').find('thead th').each(function(i,v) {
			if($(this).html() == '') {
				console.log(i);
				hide.push(i);
				$(this).hide();
			}
		});
		
		//hiding empty cells
		$('.scores-table').find('tbody tr').each(function(i, v) {
			$(this).find('td').each(function (i, v) {
				if (jQuery.inArray(i, hide) >= 0) {
					$(this).hide();
				}
			})
		});
		$('.scores-table').find('tfoot tr').each(function(i, v) {
			$(this).find('th').each(function (i, v) {
				if (jQuery.inArray(i, hide) >= 0) {
					$(this).hide();
				}
			})
		});
	})
})