(function() {
	$('.score-dropdown').show();

	var QUESTIONS = [{
		label: 'SATISFACTION DU PROCESSUS DE VENTE',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q3',
			label: 'PROCESSUS COMMERCIAL AU GLOBAL',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		},{
			code: 'q4',
			label: 'Reccomandation',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'APPARENCE DU SHOW-ROOM ET ACCUEIL',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q9',
			label: 'APPARENCE SHOWROOM',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q10',
			label: 'ACCUEIL',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q11_1',
			label: 'PRESENTATION & COMPORTEMENT',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q11_2',
			label: 'AMABILITE VENDEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q11_3',
			label: 'SOUHAIT & PREOCCUPATIONS',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q11_4',
			label: 'COMPETENCES VENDEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q11_5',
			label: 'RESPECT ENGAGEMENTS VENDEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'L\'ESSAI',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q12',
			label: 'RESOLUTION PROBLEMES VENDEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q13',
			label: 'ESSAI POUR DECISION',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q14',
			label: 'CONDITION ESSAI',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q16',
			label: 'DEROULEMENT DE L\'ESSAI',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'REPRISE DU VEHICULE',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q17',
			label: 'REPRISE OU RESTITUTION',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q19',
			label: 'RESTITUTION VEHICULE LEASING',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q21',
			label: 'ACTEUR DEMONSTRATION',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q22',
			label: 'EXPLICATIONS SUR LE VEHICULE',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q23',
			label: 'LIVRAISON VEHICULE',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q24',
			label: 'RESPECT DATE LIVRAISON',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q25',
			label: 'PROCESSUS LIVRAISON',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q27',
			label: 'LIEN AVEC APRES VENTE',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'SUIVI APRES LIVRAISON',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q28',
			label: 'CONTACT POST LIVRAISON',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q29',
			label: 'SUIVI APRES LIVRAISON',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q21',
			label: 'ACTEUR DEMONSTRATION',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'FINANCEMENT ET LEASING',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q30',
			label: 'TYPE SOUSCRIPTION',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q31',
			label: 'MB FINANCIAL SERVICES',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q32',
			label: 'FINANCEMENT ET LEASING',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}, {
		label: 'ATTITUDE GENERALE DU DISTRIBUTEUR',
		score: {
			cumu: 94.5,
			ref: 91.9
		},
		sub: [{
			code: 'q33_1',
			label: 'AMABILITE DISTRIBUTEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q33_2',
			label: 'RESPECT ENGAGEMENTS DISTRIBUTEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}, {
			code: 'q33_3',
			label: 'RESOLUTION PROBLEMES DISTRIBUTEUR',
			score: {
				cumu: 94.5,
				ref: 91.9
			}
		}]
	}];

	var graph_one_mode = 'repartition_des_reponses', graph_two_mode = 'age';

	var series2 = {repartition_des_reponses: {}};

	var series1 = {
		repartition_des_reponses: [{
			name: 'Repartion des r\351ponses',
			data: [
			{	name: 'Tout \340 fait satisfait(e)',	y: 40, color: '#27822d' },
			{	name: 'Plutot satisfait(e)',	y: 31, color: '#14ad1e' },
			{	name: 'Moyennement satisfait(e)',	y: 28, color: '#c8dd52' },
			{	name: 'Plutot pas satisfait(e)',	y: 14, color: '#f2a515' },
			{	name: 'Pas du tout satisfait(e)',	y: 18, color: '#cc3535' },
			{	name: 'Ne sais pas, pas de reponse',	y: 20 , color: '#999999'},
			{	name: 'Non concerne(e)',	y: 30 , color: '#dddddd'},
			]
		}],
		evolution_de_repartition: [{
			name: 'Tout \340 fait satisfait(e)', type: 'area', color: '#27822d' ,
			data: [49, 55, 60, 50, 43, 29, 77, 80]
		}, {
			name: 'Plutot satisfait(e)', type: 'area', color: '#14ad1e',
			data: [37, 25, 30, 42, 23, 69, 17, 12]
		}, {
			name: 'Moyennement satisfait(e)', type: 'area', color: '#c8dd52' ,
			data: [20, 25, 22, 12, 23, 19, 17, 9]
		}, {
			name: 'Plutot pas satisfait(e)', type: 'area', color: '#f2a515' ,
			data: [17, 12, 10, 12, 14, 29, 10, 12]
		}, {
			name: 'Pas du tout satisfait(e)', type: 'area', color: '#cc3535' ,
			data: [31, 28, 22, 13, 12, 9, 8, 3]
		}, {
			name: 'Ne sais pas, pas de reponse', type: 'area', color: '#999999',
			data: [11, 32, 10, 13, 14, 9, 8, 6]
		}],
		evolutions_scores: [{
			name: 'score',
			data: [80, 84, 85, 86, 88, 83, 85, 86]
		}],
		comparatifs: [{
			name: 'Etablissement',
			data: [94.3], color: '#00adef'
		}, {
			name: 'Groupe de r\351f\351r\351nce',
			data: [92], color: '#333'
		}]
	};

	series2.repartition_des_reponses = {
		age:[{
			name: '18-24 ans',
			data: [200], color: '#a3c2c2'
		}, {
			name: '25-34 ans',
			data: [240], color: '#85adad'
		}, {
			name: '35-49 ans',
			data: [231], color : '#669999'
		}, {
			name: '50-64 ans',
			data: [230], color: '#527a7a'
		}, {
			name: '65 ans et plus',
			data: [52], color : '#3d5c5c'
		}],
		sexe: [{
			name: 'Homme',
			data: [523], color: '#94b8b8'
		}, {
			name: 'Femme',
			data: [494], color : '#527a7a'
		}], //  / J'utilise principalement ce véhicule à titre professionnel / J'utilise ce véhicule à peu près autant à titre personnel qu'à titre professionnel / Je ne sais pas (encore)
		usage_vehicule: [{
			name: 'J\'utilise principalement ce v\351hicule \340 titre personnel',
			data: [182], color: '#a3c2c2'
		},{
			name: 'J\'utilise ce v\351hicule \340 peu pr\350s autant \340 titre personnel qu\'\340 titre professionnel',
			data: [201], color: '#75a3a3'
		},{
			name: 'Je ne sais pas (encore)',
			data: [170], color: '#5c8a8a'
		}],//Oui / Non, elle a remplacé un véhicule d'une autre marque / Non, je l'ai achetée comme véhicule supplémentaire / Non, c'est le premier véhicule acheté dans le foyer
		remplacement: [{
			name: 'Oui',
			data: [250], color: '#b3cccc'
		},{
			name: 'Non, elle a remplac\351 un v\351hicule d\'une autre marque',
			data: [220], color: '#85adad'
		},{
			name: 'Non, je l\'ai achet\351e comme v\351hicule suppl\351mentaire',
			data: [180], color: '#5c8a8a'
		},{
			name: 'Non, c\'est le premier v\351hicule achet\351 dans le foyer',
			data: [230], color: '#476b6b'
		}],//Moins d'un an / 1 an / 2 ans / 3 ans / 4 ans / 5 à 10 ans / 10 à 20 ans / 20 ans et plus / Je ne sais pas, pas de réponse
		anciennete_client: [{
			name: 'Moins d\'un an',
			data: [140], color: '#e0ebeb'
		}, {
			name: '1 an',
			data: [180], color: '#c2d6d6'
		}, {
			name: '2 ans',
			data: [260], color: '#a3c2c2'
		}, {
			name: '3 ans',
			data: [140], color: '#85adad'
		}, {
			name: '4 ans',
			data: [180], color: '#669999'
		}, {
			name: '5 \340 10 ans',
			data: [260], color: '#527a7a'
		}, {
			name:'10 \340 20 ans',
			data: [240], color: '#3d5c5c'
		}, {
			name:'20 ans et plus',
			data: [140], color: '#293d3d'
		}, {
			name:'Je ne sais pas, pas de r\351ponse',
			data: [140], color: '#141f1f'
		}]
	};
	series2.evolution_de_repartition = {
		age:[{
			name: '18-24 ans',
			data: [4,1, 2, 3, 0, 2, 4, 2], color: '#a3c2c2'
		}, {
			name: '25-34 ans',
			data: [2, 0, 2, 4, 5, 2, 3, 4], color: '#85adad'
		}, {
			name: '35-49 ans',
			data: [5, 6, 7, 8, 2, 5, 2, 1], color : '#669999'
		}, {
			name: '50-64 ans',
			data: [0, 0, 2, 1, 1, 0, 0, 2], color: '#527a7a'
		}, {
			name: '65 ans et plus',
			data: [2, 0, 0, 0, 2, 0, 0, 1], color : '#3d5c5c'
		}],
		sexe: [{
			name: 'Homme',
			data: [20, 31, 32, 29, 14, 29, 34], color: '#94b8b8'
		}, {
			name: 'Femme',
			data: [10, 11, 24, 2, 3, 1, 14], color: '#527a7a'
		}],
		usage_vehicule: [{
			name: 'J\'utilise principalement ce v\351hicule \340 titre personnel',
			data: [44, 22, 44, 22, 41, 44, 24, 22], color: '#a3c2c2'
		},{
			name: 'J\'utilise ce v\351hicule \340 peu pr\350s autant \340 titre personnel qu\'\340 titre professionnel',
			data: [24, 44, 24, 23, 10, 24, 40, 31], color: '#75a3a3'
		},{
			name: 'Je ne sais pas (encore)',
			data: [14, 12, 10, 5, 5, 4, 12, 13], color: '#5c8a8a'
		}],
		remplacement: [{
			name: 'Oui',
			data: [40, 32, 32, 40, 42, 45, 42, 38], color: '#b3cccc'
		},{
			name: 'Non, elle a remplac\351 un v\351hicule d\'une autre marque',
			data: [10, 20, 23, 24, 45, 23, 23, 19], color: '#85adad'
		},{
			name: 'Non, je l\'ai achet\351e comme v\351hicule suppl\351mentaire',
			data: [5, 6, 7, 10, 20, 21, 19, 12], color: '#5c8a8a'
		},{
			name: 'Non, c\'est le premier v\351hicule achet\351 dans le foyer',
			data: [2, 2, 4, 2, 1, 4, 5, 10], color: '#476b6b'
		}],
		anciennete_client: [{
			name: 'Moins d\'un an',
			data: [20, 21, 22, 14, 11, 23, 10], color: '#e0ebeb'
		}, {
			name: '1 an',
			data: [4, 3, 4, 10, 9, 11, 14, 10], color: '#c2d6d6'
		}, {
			name: '2 ans',
			data: [5, 6, 7, 3, 5, 2, 4, 1], color: '#a3c2c2'
		}, {
			name: '3 ans',
			data: [4, 4, 5, 0, 10, 14, 11, 20], color: '#85adad'
		}, {
			name: '4 ans',
			data: [2, 2, 2, 2, 2, 2, 1, 1], color: '#669999'
		}, {
			name: '5 \340 10 ans',
			data: [4, 8, 6, 4, 4, 2, 6, 2], color: '#527a7a'
		}, {
			name:'10 \340 20 ans',
			data: [10, 14, 2, 2,  4 ,3], color: '#3d5c5c'
		}, {
			name:'20 ans et plus',
			data: [0, 1, 2, 0, 0, 0, 2,1 ], color: '#293d3d'
		}, {
			name:'Je ne sais pas, pas de r\351ponse',
			data: [14, 15, 20, 14, 3, 4, 0, 10], color: '#141f1f'
		}]
	};
	series2.evolutions_scores = {
		age:[{
			name: '18-24 ans',
			data: [78, 81, 82, 79, 81, 80, 82, 83], color: '#a3c2c2'
		}, {
			name: '25-34 ans',
			data: [77, 78, 81, 79, 77, 82, 81, 80], color: '#85adad'
		}, {
			name: '35-49 ans',
			data: [80, 76, 86, 90, 88, 87, 85, 87], color : '#669999'
		}, {
			name: '50-64 ans',
			data: [82, 80, 79, 82, 81, 83, 81, 84], color: '#527a7a'
		}, {
			name: '65 ans et plus',
			data: [83, 84, 81, 82, 82, 83, 84, 78], color : '#3d5c5c'
		}],
		sexe: [{
			name: 'Homme',
			data: [86, 85, 88, 87, 88, 82, 81, 82]
		}, {
			name: 'Femme',
			data: [81, 79, 82, 83, 84, 86, 88, 90]
		}],
		usage_vehicule: [{
			name: 'J\'utilise principalement ce v\351hicule \340 titre personnel',
			data: [77, 78, 78, 80, 81, 82, 83, 85], color: '#a3c2c2'
		},{
			name: 'J\'utilise ce v\351hicule \340 peu pr\350s autant \340 titre personnel qu\'\340 titre professionnel',
			data: [88, 85, 87, 87, 86, 87, 88, 89], color: '#75a3a3'
		},{
			name: 'Je ne sais pas (encore)',
			data: [85, 84, 86, 88, 84, 83, 82, 80], color: '#5c8a8a'
		}],
		remplacement: [{
			name: 'Oui',
			data: [81, 82, 85, 85, 82, 81, 83, 85], color: '#b3cccc'
		},{
			name: 'Non, elle a remplac\351 un v\351hicule d\'une autre marque',
			data: [83, 84, 86, 83, 83, 84, 85, 86], color: '#85adad'
		},{
			name: 'Non, je l\'ai achet\351e comme v\351hicule suppl\351mentaire',
			data: [81, 82, 83, 83, 84, 83, 82, 83], color: '#5c8a8a'
		},{
			name: 'Non, c\'est le premier v\351hicule achet\351 dans le foyer',
			data: [77, 78, 77, 79, 81, 78, 82, 84], color: '#476b6b'
		}],
		anciennete_client: [{
			name: 'Moins d\'un an',
			data: [81, 82,  84, 82, 84, 81, 82, 84], color: '#e0ebeb'
		}, {
			name: '1 an',
			data: [77, 78, 76, 78, 79, 80, 79, 80], color: '#c2d6d6'
		}, {
			name: '2 ans',
			data: [86, 81, 82, 83, 81, 82, 83, 84], color: '#a3c2c2'
		}, {
			name: '3 ans',
			data: [78, 78, 79, 80, 81, 83, 84, 82], color: '#85adad'
		}, {
			name: '4 ans',
			data: [81, 81, 82, 82, 81, 82, 81, 81], color: '#669999'
		}, {
			name: '5 \340 10 ans',
			data: [79, 79, 77, 75, 74, 72, 72, 73], color: '#527a7a'
		}, {
			name:'10 \340 20 ans',
			data: [89, 86, 88, 88, 86, 81, 86, 88], color: '#3d5c5c'
		}, {
			name:'20 ans et plus',
			data: [78, 79, 78, 77, 82,  80, 82, 83], color: '#293d3d'
		}, {
			name:'Je ne sais pas, pas de r\351ponse',
			data: [84, 80, 82, 85, 86, 86, 88, 81], color: '#141f1f'
		}]
	};
	series2.comparatifs = {
		age:[{
			name: 'Etablissement',
			data: [88, 82, 88, 83, 84], color: '#00adef'
		}, {
			name: 'Groupe de r\351f\351r\351nce',
			data: [82, 81, 85, 81, 81], color: '#333'
		}],
		ageCategories: ['18-24 ans', '25-34 ans', '35-49 ans', '50-64 ans', '65 ans et plus'],
		sexe:[{
			name: 'Etablissement',
			data: [81, 77], color: '#00adef'
		}, {
			name: 'Groupe de r\351f\351r\351nce',
			data: [82, 78], color: '#333'
		}],
		sexeCategories: ['Homme', 'Femme'],
		usage_vehicule:[{
			name: 'Etablissement',
			data: [77, 77, 88], color: '#00adef'
		}, {
			name: 'Groupe de r\351f\351r\351nce',
			data: [82, 78, 85], color: '#333'
		}],
		usage_vehiculeCategories: ['J\'utilise principalement ce v\351hicule \340 titre personnel', 'J\'utilise ce v\351hicule \340 peu pr\350s autant \340 titre personnel qu\'\340 titre professionnel', 'Je ne sais pas (encore)'],
		remplacement: [{
			name: 'Etablissement',
			data: [81, 77, 78, 84], color: '#00adef'
		}, {
			name: 'Groupe de r\351f\351r\351nce',
			data: [82, 78, 82, 72], color: '#333'
		}],
		remplacementCategories: ['Oui', 'Non, elle a remplac\351 un v\351hicule d\'une autre marque', 'Non, je l\'ai achet\351e comme v\351hicule suppl\351mentaire', 'Non, c\'est le premier v\351hicule achet\351 dans le foyer'],
		anciennete_client: [{
			name: 'Etablissement', color: '#00adef',
			data: [78, 77, 77, 80, 81, 82, 76, 77]
		}, {
			name: 'Groupe de r\351f\351r\351nce', color: '#333',
			data: [88, 84, 77, 82, 77, 72, 88, 82]
		}],
		anciennete_clientCategories: ['1 an', '2 ans', '3 ans', '4 ans', '5 \340 10 ans', '10 \340 20 ans', '20 ans et plus', 'Je ne sais pas, pas de r\351ponse']
	};

	$('#questionSelectionList').append('<ul class="questions"></ul>');

	$.each(QUESTIONS, function(i, q) {
		$('#questionSelectionList ul').append('<li><a role="button"><span class="icon entypo-right-open-mini"></span> ' + q.label + ' <span class="cumu-score">'+q.score.ref+'</span> <span class="ref-score">'+q.score.cumu+'</span></a></li>');
	});

	$('#questionSelectionList ul:first li').each(function(i, l) {
		var $this = $(this);
		$this.append('<ul class="sub-questions"></ul>');
		$.each(QUESTIONS[i].sub, function(i, q) {
			$this.find('ul').append('<li><b>'+q.code+'</b> ' + q.label+ ' <span class="cumu-score">'+q.score.ref+'</span> <span class="ref-score">'+q.score.cumu+'</span></li>');
		});
	});

	$('#questionSelectionList .questions a').click(function() {
		$(this).parent().find('.sub-questions').slideToggle();

		if ($(this).parent().find('.sub-questions').hasClass('expanded')) {
			$(this).parent().find('.icon').attr('class','icon entypo-right-open-mini');
			$(this).parent().find('.sub-questions').removeClass('expanded');
		} else {
			$(this).parent().find('.icon').attr('class','icon entypo-up-open-mini');
			$(this).parent().find('.sub-questions').addClass('expanded');
		}
	});

	$('.subquestions-toggle-show').click(function() {
		$('.sub-questions').slideDown().addClass('expanded');
		$('.questions .icon').attr('class','icon entypo-up-open-mini');
	});

	$('.subquestions-toggle-hide').click(function() {
		$('.sub-questions').slideUp();
		$('.sub-questions').removeClass('expanded');
		$('.questions .icon').attr('class','icon entypo-right-open-mini');
	});

	$('.sub-questions li').click(function() {
		var selectedQuestio = ($(this).find('b').text());
		$('#selectedQuestionLabel').text(' : ' + selectedQuestio.toUpperCase());
		$('.sub-questions li').removeClass('active');
		$(this).addClass('active');
		$('.division.step-2').removeClass('hidden').addClass('col-md-7');
		$('.division.step-1').removeClass('col-md-12').addClass('col-md-5');

		renderDizGraphs();

	});
	
	$('.subquestions-toggle-show').click();
	
	$('.select-graph-one').click(function() {

		$('.select-graph-one').parent().removeClass('active-tab');
		$(this).parent().addClass('active-tab');
		graph_one_mode = $(this).attr('mode');

		renderDizGraphs();
	})

	$('.graph-two-tabs .btn').click(function(){

		$('.graph-two-tabs .btn').removeClass('btn-primary').addClass('btn-default');
		$(this).addClass('btn-primary');
		graph_two_mode= $(this).attr('mode');
		
		emulateSideLoading(function() {
			if (graph_one_mode == 'repartition_des_reponses' ) {
				createRepartionBars('#secondChartContainer', series2.repartition_des_reponses[graph_two_mode]);
			} else if  (graph_one_mode == 'evolution_de_repartition') { 
				createRepartitionEvo('#secondChartContainer', series2.evolution_de_repartition[graph_two_mode]);
			} else if (graph_one_mode == 'evolutions_scores') {
				createScoresEvo('#secondChartContainer', series2.evolutions_scores[graph_two_mode]);
			} else if (graph_one_mode == 'comparatifs'){
				createComparatifsBar2('#secondChartContainer', series2.comparatifs[graph_two_mode], series2.comparatifs[graph_two_mode + 'Categories']);
			}
		});
	})

	$('#firstChartContainer').on('pointOnPieCliked', function(event, origin) {
		$.each(series2[graph_one_mode][graph_two_mode], function (i, s) {
			s.color = shadeColor(origin.color, 10 -(10 * i));
			//randomize values
			s.data[0] = Math.floor(Math.random() * 200);
		})
		createRepartionBars('#secondChartContainer', series2[graph_one_mode][graph_two_mode]);
	})

	$('#firstChartContainer').on('pointOnAreaCliked', function(event, origin) {
		$.each(series2[graph_one_mode][graph_two_mode], function (i, s) {
			s.color = shadeColor(origin.color, 10 -(10 * i));
			//randomize values
			s.data[0] = Math.floor(Math.random() * 200);
		})
		createRepartitionEvo('#secondChartContainer', series2[graph_one_mode][graph_two_mode]);
	})
	
	var graph_level_fullscreen = null;

	$('.go-fullscreen').click(function() {
		graph_level_fullscreen = ($(this).attr('level'));
	});

	$('#seeInFullScreenDialog').on('show.bs.modal', function() {
		var h = $(window).height();
		$('#seeInFullScreenGraph').html(' ');
		$('#seeInFullScreenGraph').css('height', (h-160) + 'px');
	})

	$('#seeInFullScreenDialog').on('hide.bs.modal', function() {

	})

	$('#seeInFullScreenDialog').on('shown.bs.modal', function() {
		if (graph_one_mode == 'evolution_de_repartition') {
			if (graph_level_fullscreen == 1)
				createRepartitionEvo('#seeInFullScreenGraph', series1.evolution_de_repartition, true);
			if (graph_level_fullscreen == 2)
				createRepartitionEvo('#seeInFullScreenGraph', series2.evolution_de_repartition[graph_two_mode]);
		}
		if (graph_one_mode == 'evolutions_scores') {
			if (graph_level_fullscreen == 1)
				createScoresEvo('#seeInFullScreenGraph', series1.evolutions_scores, true);
			if (graph_level_fullscreen == 2)
				createScoresEvo('#seeInFullScreenGraph', series2.evolutions_scores[graph_two_mode]);
		}
	})

	$('.time-line-cat-selector button').click(function() {
		var mode = $(this).attr('mode'), graph_level_graph = $(this).attr('level');
		if (mode == 'day') {
			var categories =  ['jan 2', 'jan 3', 'jan 4', 'jan 5', 'jan 6',  'jan 7',  'jan 8',  'jan 9'];
		} else if (mode == 'month') {
			var categories = ['jul 16', 'auo 16', 'sep 16','oct 16','nov 16','dec 16, jan 17'];
		} else {
			var categories = ['2016', '2017'];
		}
		if (graph_one_mode == 'evolution_de_repartition') {
			if (graph_level_graph == 1) {
				createRepartitionEvo('#seeInFullScreenGraph', series1.evolution_de_repartition, true, categories);
			}
			if (graph_level_graph == 2) {
				createRepartitionEvo('#seeInFullScreenGraph', series2.evolution_de_repartition[graph_two_mode], false, categories);
			}
		}
	})


	function emulateSideLoading(callback) {
		$('.side-loading-indicator').show();
		setTimeout(function() {
			$('.side-loading-indicator').hide();
			callback();
		}, 600);
	}

	function renderDizGraphs(container) {
		emulateSideLoading(function() {
			$('.time-line-cat-selector').hide();
			if (graph_one_mode == 'repartition_des_reponses') {
				createRepartionPie('#firstChartContainer', series1.repartition_des_reponses);
				createRepartionBars('#secondChartContainer', series2.repartition_des_reponses[graph_two_mode]);
			} else if  (graph_one_mode == 'evolution_de_repartition') {
				createRepartitionEvo('#firstChartContainer', series1.evolution_de_repartition, true);
				createRepartitionEvo('#secondChartContainer', series2.evolution_de_repartition[graph_two_mode]);
				$('.time-line-cat-selector').show();
			} else if  (graph_one_mode == 'evolutions_scores') {
				createScoresEvo('#firstChartContainer', series1.evolutions_scores);
				createScoresEvo('#secondChartContainer', series2.evolutions_scores[graph_two_mode]);
				$('.time-line-cat-selector').show();
			} else {
				createComparatifsBar('#firstChartContainer', series1.comparatifs);
				createComparatifsBar2('#secondChartContainer', series2.comparatifs[graph_two_mode], series2.comparatifs[graph_two_mode + 'categories']);
			}
		})
	}

}())

// color shader.
function shadeColor(color, percent) {

	var R = parseInt(color.substring(1,3),16);
	var G = parseInt(color.substring(3,5),16);
	var B = parseInt(color.substring(5,7),16);

	R = parseInt(R * (100 + percent) / 100);
	G = parseInt(G * (100 + percent) / 100);
	B = parseInt(B * (100 + percent) / 100);

	R = (R<255)?R:255;  
	G = (G<255)?G:255;  
	B = (B<255)?B:255;  

	var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
	var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
	var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

	return "#"+RR+GG+BB;
}