var BU_ACTIVE = 'Vente Cars';

(function() {
	var $leftnavmenu = $('#myNavmenu');
	var $navbar = $('.navbar');
	var $maincontent = $('.main-content');
	
	$('.left-menu-toggle').click(function() {
		$leftnavmenu.toggleClass('not-expanded');
		$navbar.toggleClass('c-margin-left');
		$maincontent.toggleClass('c-margin-left');
	});
	
	if ($(window).width() <= 765) {
			hideLeftMenu();
	}
	
	$(window).resize(function() {
		if ($(window).width() <= 765) {
			hideLeftMenu();
		}
	});
	
	function hideLeftMenu() {
		$leftnavmenu.addClass('not-expanded');
		$navbar.removeClass('c-margin-left');
		$maincontent.removeClass('c-margin-left');
	}
	var start = moment().subtract(29, 'days');
	var end = moment();

	function cb(start, end) {
		$('#reportrange span').html(start.format('MMMM , YYYY') + ' - ' + end.format('MMMM , YYYY'));
	}
	
	$('input[name="daterange"]').daterangepicker({
		startDate: start,
		endDate: end,
		opens: 'left',
		showDropdowns: true,
		 linkedCalendars: false,
		locale: {
			"applyLabel": "Appliquer",
			"cancelLabel": "Annuler",	
			"customRangeLabel" : 'P\351riode au choix'
		},
		ranges: {
			'Cumul glissant': [moment().startOf('month'), moment().endOf('month')],
			'Cumul annuel': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'Mensuel': [moment().startOf('month'), moment().endOf('month')]
		}
	}, cb);

	cb(start, end);
	
	$('.navmenu-nav').find('a').click(function() {
		var target = $(this).attr('target');
		$(target).slideToggle();
	});
	
	$('.bu-navbar').find('li').click(function() {
		BU_ACTIVE = $(this).text();
		$('.bu-navbar').find('li').removeClass('active');
		$(this).addClass('active');
		$('body').trigger('BU_CHANGE');
		emulateLoading();
	})
	
}())


function emulateLoading() {
	$('.main-content-view').hide();
	$('#loadingIndicator').show();
	setTimeout(function() {
		$('.main-content-view').show();
		$('#loadingIndicator').hide();
	}, 600);
}