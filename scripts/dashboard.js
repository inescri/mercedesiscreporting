$(document).ready(function() {
	var PERIMETER_TYPE = 4;
	
	createRing('#scoreISCRing',150, 180, {
		myScore: [95, 5]
	}, ['#00ADEF', '#dfdfdf']);

	createRing('#scoreNPSRing',150, 180, {
		myScore: [50, 50]
	}, ['#00ADEF', '#dfdfdf']);

	setTimeout(function() {
		$('.main-content-view').css('opacity',1);
	},1000);

	var isg_categories = [
		'Global ISC',
		'Accueil',
		'Comp\351tences Vendeur',
		'D\351roulement de l\'essai',
		'Explications sur le v\351hicule',
		'Suivi apres livraison',
		'Financement et leasing'
	],  
	isg_factors_vn_carsl = {
		weakness: {
			short_labels: [
				'Presentation & comportement',
				'Condition essai',
				'Reprise ou restitution',
				'Livraison vehicule'
			],
			long_labels: [
				'Dans quelle mesure avez-vous \351t\351 satisfait(e) de votre vendeur concernant sa pr\351sentation et son comportement ?',
				'Dans quelles conditions avez-vous r\351alis\351 votre essai ?',
				'Avez-vous b\351n\351fici\351 d\'une reprise pour votre v\351hicule pr\351c\351dent ou restitu\351 un v\351hicule en leasing chez votre distributeur ?',
				'Dans quelle mesure avez-vous \351t\351 satisfait(e) de l\'\351tat du v\351hicule au moment de la livraison ?'
			],
			star: [false, false, false]
		},
		improve: {
			short_labels: [
				'Preparation premier RDV',
				'Enchantement client',
				'Suivi apres livraison'
			],
			long_labels: [
				'Si votre voiture n\351cessitait une r\351paration, a-t-elle \351t\351 effectu\351e lors du premier rendez-vous ?',
				'S\'est-il produit quelque chose qui vous ait agr\351ablement surpris(e) ou meme ravi(e) en tant que client(e) chez votre r\351parateur agr\351\351 ?',
				'Avez-vous souscrit un contrat de financement ou de leasing pour acheter votre nouveau v\351hicule, chez votre distributeur ?'
			],
			star: [false, false, true]
		},
		strength: {
			short_labels: [
				'Respect date livraison',
				'Lien avec apres vente',
				'Accueil',
				'Statisfaction star export'
			],
			long_labels: [
				'Globalement avez-vous \351t\351 satisfait(e) de l’ensemble du process de livraison du v\351hicule ? ',
				'Avez-vous \351t\351 pr\351sent\351(e) \340 une personne du service apr\350s-vente durant la livraison du v\351hicule ?',
				'Dans quelle mesure avez-vous \351t\351 satisfait(e) de l\'accueil que vous avez reçu au sein du showroom ?',
				'Avez-vous \351t\351 satisfait(e) de l’accompagnement de ce Star Expert Mercedes-Benz (Expert Produit) ?'
			],
			star: [false, false, true]
		}
	};

	createFactorsTable(isg_factors_vn_carsl);

	$('body').on("BU_CHANGE", function() {

		if (BU_ACTIVE == 'Vente Cars') {
			 isg_categories = [
				'Global ISC',
				'Accueil',
				'Comp\351tences Vendeur',
				'D\351roulement de l\'essai',
				'Explications sur le v\351hicule',
				'Suivi apres livraison',
				'Financement et leasing'
			];

			isg_factors = isg_factors_vn_carsl;
		}

		if (BU_ACTIVE == 'Service Cars' ) {

			isg_categories = [
				'Global ISC', 'Prise de Rendez-vous', 'Conseiller de clientele SAV', 'L\'infrastructure', 'R\351cup\351ration du v\351hicule' , 'Qualit\351 du suivi apres intervention'
			];

			isg_factors = {
				weakness: {
					short_labels: [
						'Presentation & comportement',
						'Condition essai',
						'Reprise ou restitution',
						'Salle d\'attente'
					],
					long_labels: [
						'Dans quelle mesure avez-vous \351t\351 satisfait(e) de votre vendeur concernant sa pr\351sentation et son comportement ?',
						'Dans quelles conditions avez-vous r\351alis\351 votre essai ?',
						'Avez-vous b\351n\351fici\351 d\'une reprise pour votre v\351hicule pr\351c\351dent ou restitu\351 un v\351hicule en leasing chez votre distributeur ?',
						'Comment \351valueriez-vous la salle d\'attente (confort, sièges, commodit\351s) ?'
					],
					star: [false, false, false]
				},
				improve: {
					short_labels: [
						'Suggestions',
						'Amabilite distributeur',
						'Recommandation',
						'Politesse CS'
					],
					long_labels: [
						'Souhaiteriez-vous donner \340 votre distributeur d\'autres informations ou suggestions d\'am\351lioration ?',
						'Dans quelle mesure avez-vous \351t\351 satisfait(e) de votre distributeur concernant l\'amabilit\351 ?',
						'A l\'avenir, recommanderiez-vous ce r\351parateur agr\351\351 \340 un ami ou \340 un membre de votre famille pour des travaux d\'entretien ou de r\351paration ? ',
						'Comment \351valueriez-vous la politesse du conseiller service, r\351ceptionnaire ?'
					],
					star: [false, false, false, false]
				},
				strength: {
					short_labels: [
						'Respect date livraison',
						'Lien avec apres vente',
						'Temps intervention'					],
					long_labels: [
						'Globalement avez-vous \351t\351 satisfait(e) de l’ensemble du process de livraison du v\351hicule ? ',
						'Avez-vous \351t\351 pr\351sent\351(e) \340 une personne du service apr\350s-vente durant la livraison du v\351hicule ?',
						'Concernant la qualit\351 de service, comment \351valueriez-vous le temps n\351cessaire pour effectuer les travaux sur votre v\351hicule ?'
					],
					star: [false, false, false]
				}
			};
		}

		if (BU_ACTIVE == 'Vente Vans') {
			isg_categories = [
				'Satisfaction globale',
				'Relationnel vendeur',
				'Propos d\'essai',
				'D\351roulement de l\'essai',
				'Livraison du v\351hicule',
				'Contact post-livraison',
				'Suivi apres livraison'
			];

			isg_factors = {
				weakness: {
					short_labels: [
						'Vendeur financement',
						'Propo essai',
						'Financement',
						'Telephone'
					],
					long_labels: [
						'Comment avez-vous jug\351 le vendeur ou le conseiller en financement qui vous a vendu votre financement ou votre location longue dur\351e ?',
						'Vous a-t-on propos\351 un essai ?',
						'Dans quelle mesure avez-vous \351t\351 satisfait(e) du produit que vous avez choisi ?',
						'Avez-vous \351t\351 satisfait(e) de la qualit\351 des contacts par t\351l\351phone ?'
					],
					star: [false, true, false]
				},
				improve: {
					short_labels: [
						'Reclamation',
						'Ambiance livraison',
						'Satisfaction hall',
						'Fidelite'
					],
					long_labels: [
						'Avez-vous fait une r\351clamation \340 propos du processus de vente ?',
						'Plus pr\351cis\351ment, dans quelle mesure avez-vous \351t\351 satisfait(e) de l\'ambiance durant la proc\351dure de remise du v\351hicule ?',
						'A propos de l\'am\351nagement et de la pr\351sentation du hall d’exposition, \351tiez-vous :',
						'Si vous aviez \340 racheter un V\351hicule Utilitaire Mercedes-Benz, iriez-vous de nouveau chez le meme distributeur ?'
					],
					star: [false, false, false, true]
				},
				strength: {
					short_labels: [
						'Fidelite',
						'Relationnel vendeur',
						'Competences vendeur'
					],
					long_labels: [
						'Si vous aviez \340 racheter un V\351hicule Utilitaire Mercedes-Benz, iriez-vous de nouveau chez le meme distributeur ?',
						'Plus pr\351cis\351ment, dans quelle mesure avez-vous \351t\351 satisfait(e) de l\'ambiance durant la proc\351dure de remise du v\351hicule ?',
						'Plus pr\351cis\351ment, dans quelle mesure avez-vous \351t\351 satisfait(e) de ses connaissances et comp\351tences ?'
					],
					star: [false, false, false]
				}
			};

		}

		if (BU_ACTIVE  == 'Service Trucks') {
			//service trucks
			isg_factors ={
				weakness: {
					short_labels: [
						'Horaires ouverture atelier',
						'Traitement reclamation garantie',
						'Contact post livraison',
						'Dispo atelier'
					],
					long_labels: [
						'Quel est votre niveau de satisfaction concernant les jours et les heures d\'ouverture de l\'atelier ?',
						'Quel est votre niveau de satisfaction concernant le traitement des r\351clamations formul\351es au titre de la garantie ?',
						'Avez-vous \351t\351 contact\351(e) par votre atelier au sujet de l\'intervention, dans les jours qui ont suivi votre passage ?',
						'De mani\350re plus pr\351cise, quel est votre niveau de satisfaction concernant la disponibilit\351 de l\'atelier quand vous en avez besoin ?'
					],
					star: [false, false, false]
				},
				improve: {
					short_labels: [
						'Duree immobilisation',
						'Fidelite',
						'Conseils magasin',
						'Recommandation'
					],
					long_labels: [
						'Quel est votre niveau de satisfaction concernant le temps total d\'immobilisation?',
						'Lors d\'une prochaine intervention, iriez-vous \340 nouveau chez ce r\351parateur agr\351\351 Mercedes-Benz ?',
						'Quel est votre niveau de satisfaction concernant les conseils du magasinier ?',
						'Recommanderiez-vous cet atelier Mercedes-Benz \340 un collègue ou une connaissance ?'
					],
					star: [false, true, false, true]
				},
				strength: {
					short_labels: [
						'Satisfaction globale',
						'Depannage urgence',
						'Date restitution'
					],
					long_labels: [
						'En g\351n\351ral, dans quelle mesure êtes-vous satisfait(e) du service de l\'atelier Mercedes-Benz lors de cette derni\350re intervention ?',
						'Quel est votre niveau de satisfaction concernant le service de d\351pannage d\'urgence en cas de panne ?',
						'De mani\350re plus pr\351cise, quel est votre niveau de satisfaction concernant les informations concernant la date de restitution du v\351hicule ?'
					],
					star: [true, false, false]
				}
			};
		}

		if (BU_ACTIVE == 'Service Vans') {
			//service vans 
			isg_factors = {
				weakness: {
					short_labels: [
						'Rapport qualite-prix',
						'Dispo pieces rechange'
					],
					long_labels: [
						'Quel est votre niveau de satisfaction concernant le cout des r\351parations et d\'entretien par rapport \340 la prestation rendue ?',
						'Quel est votre niveau de satisfaction concernant la disponibilit\351 des pi\350ces de rechange ?'
					],
					star: [false, false, false]
				},
				improve: {
					short_labels: [
						'Attitude reception',
						'Fidelite'
					],
					long_labels: [
						'De mani\350re plus pr\351cise, quel est votre niveau de satisfaction concernant l\'attitude et le comportement du personnel de la r\351ception atelier de mani\350re g\351n\351rale ?',
						'Lors d\'une prochaine intervention, iriez-vous \340 nouveau chez ce r\351parateur agr\351\351 Mercedes-Benz ? '
					],
					star: [false, true, false]
				},
				strength: {
					short_labels: [
						'Recommendation',
						'Delai restitution',
						'Tarifs horaires'
					],
					long_labels: [
						'Recommanderiez-vous cet atelier Mercedes-Benz \340 un coll\351gue ou une connaissance ?',
						'De mani\350re plus pr\351cise, quel est votre niveau de satisfaction concernant le respect du d\351lai de restitution du v\351hicule ?',
						'Quel est votre niveau de satisfaction concernant les tarifs horaires de main d\'oeuvre de l\'atelier ?'
					],
					star: [true, false, false]
				}
			};
		}

		if (BU_ACTIVE == 'Vente Trucks') {

			// vente trucks
			isg_factors = {
				weakness: {
					short_labels: [
						'PDV - formation',
						'Qualite prepa vehicule',
						'Investissement vendeur',
						'Traitement reclamation'
					],
					long_labels: [
						'Quel est votre niveau de satisfaction concernant la capacit\351 du point de vente \340 proposer des formations Eco conduite aux chauffeurs lors de la vente et la livraison de votre camion Mercedes-Benz ?',
						'Et plus particuli\350rement concernant la qualit\351 de la pr\351paration du v\351hicule, vous diriez que vous êtes…',
						'Quel est votre niveau de satisfaction concernant l\'investissement du vendeur dans la relation clients lors de la vente et la livraison de votre camion Mercedes-Benz ?',
						'Le cas \351ch\351ant, comment \351valueriez-vous le traitement des r\351clamations en ce qui concerne le processus de vente et de livraison ? Vous diriez que vous êtes … '
					],
					star: [false, false, false]
				},
				improve: {
					short_labels: [
						'Conseil mise en main',
						'Rapidite sous..APV',
						'Suivi apres livraison'
					],
					long_labels: [
						'Lors de la mise en main de votre v\351hicule, vous a-t-on donn\351 des conseils sur l\'utilisation et l\'entretien de votre v\351hicule ?',
						'Quel est votre niveau de satisfaction concernant la rapidit\351 \340 g\351n\351rer l\'offre et la souscription au contrat de r\351paration, d\'entretien ou d\'extension de garantie ?',
						'Comment \351valueriez-vous le suivi clientèle de votre distributeur ou vendeur depuis la livraison de votre v\351hicule, vous diriez que vous êtes … '
					],
					star: [false, false, true]
				},
				strength: {
					short_labels: [
						'Recommandation',
						'Satisfaction globale',
						'Propo essai',
						'Gestion formalite adm'
					],
					long_labels: [
						'Si un ami, une connaissance ou une relation d’affaires vous le demandait, recommanderiez-vous ce distributeur ?',
						'D\'une mani\350re g\351n\351rale, comment \351valueriez-vous la prestation commerciale de votre \351tablissement distributeur lors de la vente et de la livraison de votre camion Mercedes-Benz ? ',
						'Vous a-t-on propos\351 d\'effectuer un essai avant l\'achat de votre v\351hicule ? ',
						'Et plus particulièrement concernant la gestion des formalit\351s administratives, vous diriez que vous etes…'
					],
					star: [true, true, false]
				}
			};
		}

		if (BU_ACTIVE == 'Service Trucks' || BU_ACTIVE == 'Vente Trucks' || BU_ACTIVE == 'Service Vans')  {
			isg_categories = [
				'Global ISC',
				'Satisfaction globale',
				'Recommandation',
				'Fid\351lit\351'
			];

		}
		createFactorsTable(isg_factors);
		$('#perimCategory').trigger('change');
	});

	createISCCompare([{
		name: 'Mon score',
		data: [95, 35.9, 71.5, 98, 32, 99, 76],
		color: '#00ADEF'
	}, {
		name: 'Mon district',
		data: [77, 83.6, 78.8, 74.5, 93.4, 99, 84.5],
		color: '#337ab7'
	}, {
		name: 'Autres districts',
		data: [67, 83.6, 78.8, 78.5, 93.4, 99, 84.5],
		color: '#999'
	},{
		name: 'Groupe de r\351f\351rence',
		data: [88, 48.9, 38.8, 39.3, 41.4, 47.0, 48.3],
		color: '#555'
	}, {
		name: 'National',
		data: [87,55.4, 33.2, 34.5, 39.7, 52.6, 75.5],
		color: '#333'
	}], isg_categories);

	$('#responseRate').highcharts({
		chart: {
			type: 'column',
			marginTop: 45,
		},
		credits: {
			enabled: false
		},
		title: {
			text: ''
		},
		xAxis: {
			categories: ['Year to date', 'Mois en cours']
		},
		yAxis: {
			min: 0,
			max: 100,
			title: {
				text: '',
			},
			stackLabels: {
				enabled: false,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			enabled: false,
			align: 'right',
			x: 0,
			verticalAlign: 'top',
			y: -10,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: false
				},
				borderColor: '#ccc'
			}
		},
		series: [{
			name: '',
			color: '#ddd',
			data: [52, 53]
		}, {
			name: '',
			data: [24, 22],
			color: '#00ADEF'
		}]
	});

	$('#iscComparisonRow').hide();
	$('#toggleISCComparison').click(function() {

		$('#iscComparisonRow').slideToggle();
	});
	
	$('#perimCategory').val(PERIMETER_TYPE);
	
	$('#perimCategory').change(function() {
		PERIMETER_TYPE = $(this).val();
		$('.perimeter-opts').hide();
		if (PERIMETER_TYPE == '4') {
			$('#districts').show();
			$('#topFlopPanel .dash-title').text("Performance Site");
			$('#topFlopPanel').removeClass("col-md-4");
			$('#topFlopPanel').addClass("col-md-8");
			$('#statisticsPanel').removeClass("col-md-8");
			$('#statisticsPanel').addClass("col-md-4");
			$('#alertantsPanel2').show();
			$('#alertantsPanel1').hide();
		}
		if (PERIMETER_TYPE == '5') {
			$('#country').show();
			$('#topFlopPanel .dash-title').text("Performance District");
			$('#topFlopPanel').removeClass("col-md-4");
			$('#topFlopPanel').addClass("col-md-8");
			$('#alertantsPanel2').show();
			$('#alertantsPanel1').hide();

			createISCCompare([{
				name: 'Mon district',
				data: [77, 83.6, 78.8, 74.5, 93.4, 99, 84.5],
				color: '#337ab7'
			},{
				name: 'Groupe de r\351f\351rence',
				data: [88, 48.9, 38.8, 39.3, 41.4, 47.0, 48.3],
				color: '#555'
			}, {
				name: 'National',
				data: [87,55.4, 33.2, 34.5, 39.7, 52.6, 75.5],
				color: '#333'
			}], isg_categories);
		} else {
			createISCCompare([{
				name: 'Mon score',
				data: [95, 35.9, 71.5, 98, 32, 99, 76],
				color: '#00ADEF'
			}, {
				name: 'Mon district',
				data: [77, 83.6, 78.8, 74.5, 93.4, 99, 84.5],
				color: '#337ab7'
			}, {
				name: 'Autres districts',
				data: [67, 83.6, 78.8, 78.5, 93.4, 99, 84.5],
				color: '#999'
			},{
				name: 'Groupe de r\351f\351rence',
				data: [88, 48.9, 38.8, 39.3, 41.4, 47.0, 48.3],
				color: '#555'
			}, {
				name: 'National',
				data: [87,55.4, 33.2, 34.5, 39.7, 52.6, 75.5],
				color: '#333'
			}], isg_categories);
		}
		if (PERIMETER_TYPE == '3') {
			$('#investors').show();
			$('#topFlopPanel').removeClass("col-md-8");
			$('#topFlopPanel').addClass("col-md-4");
			$('#alertantsPanel1').show();
			$('#alertantsPanel2').hide();

		}
		if (PERIMETER_TYPE == '2') {
			$('#transverse').show();
			$('#topFlopPanel').removeClass("col-md-8");
			$('#topFlopPanel').addClass("col-md-4");
			$('#alertantsPanel1').show();
			$('#alertantsPanel2').hide();

		}
		if (PERIMETER_TYPE == '1') {
			$('#establishments').show();
			$('#alertantsPanel1').show();
			$('#alertantsPanel2').hide();
		}
		
		if (PERIMETER_TYPE > 1) {
			$('#topFlopPanel').show();
			$('#rankingPanel').hide();
		} else {
			$('#topFlopPanel').hide();
			$('#rankingPanel').show();
		}

		// change labels to districts if National level.
		if (PERIMETER_TYPE == 5) {
			$('.slide-contain').find('.score-label').each(function(i,v) {
				$(this).text('District ' + Math.floor((i+1) * 3.3));
			});
		} else {
			$('.slide-contain').find('.score-label').each(function(i,v) {
				$(this).text('Site ' + Math.floor((i+1) * 3.3));
			});
		}
		
		emulateLoading();
	})
	
	var unslider = $('.slide-contain').unslider({
		animation: 'vertical',
		arrows: null
	});
	
	$('#brandSwitch').click(function() {
		emulateLoading();
	});
	
	$('.perimeter-opts').change(function() {
		emulateLoading();
	});
	
	$('#topFlopPanel').find('.next-page').click(function() {
		var target = $(this).attr('data-target');
		$(target).unslider('next');
	});

	$('#topFlopPanel').find('.prev-page').click(function() {
		var target = $(this).attr('data-target');
		$(target).unslider('prev');
	});

	function createFactorsTable(isg_factors) {
		var $panel = $('.weakness-strength');
		var $list_weakness = $panel.find('.weakness-strength-title.weakness').parent().find('ul');
		var $list_improve = $panel.find('.weakness-strength-title.improve').parent().find('ul');
		var $list_strength = $panel.find('.weakness-strength-title.strength').parent().find('ul');

		var list = '';
		$.each(isg_factors.weakness.short_labels, function (i, v) {
			var star = '';
			if (isg_factors.weakness.star[i] == true) {
				star = '<i class="fa fa-star"></i> '
			}
			list = list + '<li title="' + isg_factors.weakness.long_labels[i]+ '">' + star + v  +'</li>';
		})
		$list_weakness.html(list);

		var list = '';
		$.each(isg_factors.improve.short_labels, function (i, v) {
			var star = '';
			if (isg_factors.improve.star[i] == true) {
				star = '<i class="fa fa-star"></i> '
			}
			list = list + '<li title="' + isg_factors.improve.long_labels[i]+ '">' + star + v  +'</li>';
		})
		$list_improve.html(list);

		var list = '';
		$.each(isg_factors.strength.short_labels, function (i, v) {
			var star = '';
			if (isg_factors.strength.star[i] == true) {
				star = '<i class="fa fa-star"></i> '
			}
			list = list + '<li title="' + isg_factors.strength.long_labels[i]+ '">' + star + v  +'</li>';
		})
		$list_strength.html(list);
	}

	function createISCCompare (data_series, data_categories) {

		var clean_data_series = data_series;

		clean_data_series.forEach(function(data) {
			data.data.splice(0, (data.data.length - data_categories.length));
		});

		$('#iscComparison').highcharts({
			chart: {
				type: 'column'
			},
			credits: {
				enabled: false
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: data_categories,
				crosshair: true
			},
			yAxis: {
				min: 0,
				max: 100,
				title: {
					text: ''
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: clean_data_series
		});
	}

})